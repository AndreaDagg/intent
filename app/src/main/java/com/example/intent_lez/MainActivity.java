package com.example.intent_lez;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bottoneGO;
   // android.widget.TextView r;
    EditText textField;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottoneGO = (Button) findViewById(R.id.GO);
        textField = (EditText)findViewById(R.id.TEXT);

        bottoneGO.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        String uri = textField.getText().toString();
        Toast.makeText(MainActivity.this,"Starting - " + uri, Toast.LENGTH_SHORT).show();
        try{
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(intent);

        }catch (Exception e){
            Toast.makeText(MainActivity.this, "Error - " + e, Toast.LENGTH_LONG);
        }
            

    }
}
